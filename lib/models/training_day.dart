import 'package:meta/meta.dart';

import '../models/models.dart';

@immutable
class TrainingDay {
  final int dayNum;
  final String dayName;
  final String date;
  final List<Training> trainings;

  const TrainingDay({this.dayNum, this.dayName, this.date, this.trainings});

  @override
  String toString() {
    return 'TrainingDay{dayNum: $dayNum, dayName: $dayName, date: $date, trainings: $trainings}';
  }
}
