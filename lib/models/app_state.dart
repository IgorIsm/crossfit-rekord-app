import 'package:meta/meta.dart';

import 'models.dart';

@immutable
class AppState {
  final List<TrainingDay> trainingDays;
  final bool trainingDaysLoading;

  AppState({this.trainingDays, this.trainingDaysLoading});

  AppState.initial(
      [this.trainingDays = const [], this.trainingDaysLoading = true]);

  AppState copyWith(
      {List<TrainingDay> trainingDays, bool trainingDaysLoading}) {
    return new AppState(
        trainingDays: trainingDays ?? this.trainingDays,
        trainingDaysLoading: trainingDaysLoading ?? this.trainingDaysLoading);
  }
}
