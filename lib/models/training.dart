import 'package:meta/meta.dart';

import '../models/models.dart';

@immutable
class Training {
  final String time;
  final List<Participant> participants;

  const Training({this.time, this.participants = const []});

  @override
  String toString() {
    return 'Training{time: $time, participants: $participants}';
  }
}
