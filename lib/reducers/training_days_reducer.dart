import 'package:redux/redux.dart';

import '../actions/actions.dart';
import '../models/training_day.dart';

final trainingDaysReducer = combineReducers<List<TrainingDay>>([
  new TypedReducer<List<TrainingDay>, TrainingDaysLoadedAction>(
      _setLoadedTrainingDays)
]);

final trainingDaysLoadingReducer = combineReducers<bool>(
    [new TypedReducer<bool, TrainingDaysLoadedAction>(_setTrainingDaysLoaded)]);

List<TrainingDay> _setLoadedTrainingDays(
    List<TrainingDay> trainingDays, TrainingDaysLoadedAction action) {
  return action.trainingDays;
}

bool _setTrainingDaysLoaded(
    bool trainingDays, TrainingDaysLoadedAction action) {
  return false;
}
