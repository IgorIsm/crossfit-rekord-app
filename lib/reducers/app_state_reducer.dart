import '../models/models.dart';
import '../reducers/training_days_reducer.dart';

AppState appReducer(AppState state, action) {
  return new AppState(
      trainingDays: trainingDaysReducer(state.trainingDays, action),
      trainingDaysLoading:
          trainingDaysLoadingReducer(state.trainingDaysLoading, action));
}
