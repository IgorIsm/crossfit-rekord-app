import 'dart:async';
import 'dart:core';

import '../models/models.dart';

class TrainingRepository {
  const TrainingRepository();

  Future<List<TrainingDay>> loadTrainingDays() async {
    return new Future.delayed(new Duration(seconds: 3), () {
      return const [_dayOne, _dayTwo, _dayThree];
    });
  }
}

const TrainingDay _dayOne = const TrainingDay(
    dayNum: 0,
    dayName: "Понедельник",
    date: "26 Марта",
    trainings: const [
      const Training(time: "10:00", participants: [
        const Participant(name: "Участник 1"),
        const Participant(name: "Участник 2")
      ])
    ]);

const TrainingDay _dayTwo = const TrainingDay(
    dayNum: 0,
    dayName: "Вторник",
    date: "27 Марта",
    trainings: const [
      const Training(time: "10:00", participants: [
        const Participant(name: "Участник 3"),
        const Participant(name: "Участник 4")
      ])
    ]);

const TrainingDay _dayThree = const TrainingDay(
    dayNum: 0,
    dayName: "Среда",
    date: "28 Марта",
    trainings: const [
      const Training(time: "10:00", participants: [
        const Participant(name: "Участник 5"),
        const Participant(name: "Участник 6")
      ])
    ]);
