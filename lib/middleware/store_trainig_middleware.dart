import 'package:redux/redux.dart';

import '../actions/actions.dart';
import '../models/models.dart';
import '../repositories/training_repository.dart';

List<Middleware<AppState>> createStoreTodosMiddleware([
  TrainingRepository repository = const TrainingRepository(),
]) {
  final loadTrainings = _createLoadTrainingDays(repository);

  return [
    new TypedMiddleware<AppState, LoadTrainingDaysAction>(loadTrainings),
  ];
}

Middleware<AppState> _createLoadTrainingDays(TrainingRepository repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    repository.loadTrainingDays().then(
      (trainingDays) {
        store
            .dispatch(new TrainingDaysLoadedAction(trainingDays: trainingDays));
      },
    ); //.catchError((_) => store.dispatch(new TodosNotLoadedAction()));

    next(action);
  };
}
