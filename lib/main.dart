import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'actions/actions.dart';
import 'middleware/store_trainig_middleware.dart';
import 'models/models.dart';
import 'reducers/app_state_reducer.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  final store = new Store<AppState>(
    appReducer,
    initialState: new AppState.initial(),
    middleware: createStoreTodosMiddleware(),
  );

  @override
  Widget build(BuildContext context) {
    return new StoreProvider(
        store: store,
        child: new MaterialApp(
            title: 'Flutter Demo',
            theme: new ThemeData(
              primarySwatch: Colors.blue,
            ),
            routes: {
              "/": (context) {
                return new StoreBuilder<AppState>(
                  onInit: (store) =>
                      store.dispatch(new LoadTrainingDaysAction()),
                  builder: (context, store) {
                    return new MyHomePage(title: 'Crossfit Rekord');
                  },
                );
              },
            }));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (context, vm) {
          if (vm.loading) {
            return new Scaffold(
                appBar: new AppBar(
                  title: new Text(widget.title),
                ),
                body: new Center(
                    child: new CircularProgressIndicator(value: null)));
          } else {
            final List<Widget> children = vm.trainingDays.map((trainingDay) {
                  return new Text(
                      '${trainingDay.dayName} : ${trainingDay.date}');
                }).toList() ??
                <Widget>[];
            return new Scaffold(
                appBar: new AppBar(
                  title: new Text(widget.title),
                ),
                body: new ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    final trainings =
                        vm.trainingDays[index].trainings.map((training) {
                      final participants =
                          training.participants.map((participant) {
                        return new ListTile(title: new Text(participant.name));
                      }).toList();
                      return new ExpansionTile(
                        title: new Text(training.time),
                        children: participants,
                      );
                    }).toList();
                    return new ExpansionTile(
                      title: children[index],
                      children: trainings,
                    );
                  },
                  itemCount: children.length,
                ));
          }
        });
  }
}

class _ViewModel {
  final List<TrainingDay> trainingDays;
  final bool loading;

  _ViewModel({
    @required this.trainingDays,
    @required this.loading,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return new _ViewModel(
        trainingDays: store.state.trainingDays,
        loading: store.state.trainingDaysLoading);
  }
}
