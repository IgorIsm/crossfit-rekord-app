import '../models/models.dart';

class LoadTrainingDaysAction {}

class TrainingDaysLoadedAction {
  final List<TrainingDay> trainingDays;

  TrainingDaysLoadedAction({this.trainingDays});
}
